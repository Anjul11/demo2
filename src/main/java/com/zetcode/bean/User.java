package com.zetcode.bean;

import java.util.List;

public class User {
    public List<CabEntity> getCabEntities() {
        return cabEntities;
    }

    public void setCabEntities(List<CabEntity> cabEntities) {
        this.cabEntities = cabEntities;
    }

    List<CabEntity> cabEntities;

    private String name;
    private String occupation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getAreaid() {
        return areaid;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid;
    }

    private String areaid;
}
